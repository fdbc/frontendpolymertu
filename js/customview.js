var clientesObtenidos;

function obtenerClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request  = new XMLHttpRequest();
  request.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         // Typical action to be performed when the document is ready:
         //document.getElementById("demo").innerHTML = xhttp.responseText;

         clientesObtenidos = request.responseText;
         //console.log(request.responseText);
         ProcesarClientes();
      }
  };
  request.open("GET", url, true);
  request.send();

}

function ProcesarClientes(){
      var JSONClientes = JSON.parse(clientesObtenidos);
      //var tabla = document.getElementById("t01");
      var tabla = this.shadowRoot.querySelector("#t01");
      console.log(tabla);

      for (var i =0; i <JSONClientes.value.length;i++){
        var nuevaFila = document.createElement("tr");
        var columnaNombre= document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        //console.log(JSONClientes.value[i].ContactName);
        var columnaCompany= document.createElement("td");
        columnaCompany.innerText = JSONClientes.value[i].CompanyName;
        //console.log(JSONClientes.value[i].CompanyName);
        var columnaCountry= document.createElement("td");
        columnaCountry.innerText = JSONClientes.value[i].Country;
        //console.log(JSONClientes.value[i].Country);
        var columnaFlag= document.createElement("img");
        columnaFlag.innerText = JSONClientes.value[i].Country;
        if (JSONClientes.value[i].Country=="UK"){
            columnaFlag.setAttribute("src",
                                     "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png");
        }
        else{
            columnaFlag.setAttribute("src",
                                     "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
                                     + JSONClientes.value[i].Country
                                     + ".png");
        }
        columnaFlag.classList.add("flag");
        columnaFlag.setAttribute("alt", "Flag");

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCompany);
        nuevaFila.appendChild(columnaCountry);
        nuevaFila.appendChild(columnaFlag);
        //console.log(nuevaFila);
        tabla.appendChild(nuevaFila);

      };
  }
